# Sonarqube@inria in few words

* https://sonarqube.bordeaux.inria.fr/ (at the moment)

* You should __really__ read the [documentation](https://sonarqube.bordeaux.inria.fr/pages/documentation.html):
    - Clear and synthetic presentation of the tool.
    - Few naming rules specified to help them administer the server properly.
    - Showing of static and analysis tools for C++ and Python.
    
* FYI: [SonarCloud](https://sonarcloud.io/about) also provides free hosting for open source projects.


# Use cases on a simple Heat project

The use cases below are lifted directly from an [introductory TP](https://sed-bso.gitlabpages.inria.fr/heat/sonarqube.html) created by Florent Pruvost (SED Bordeaux).

To run them:

* Go to the [Inria instance](https://sonarqube.bordeaux.inria.fr/) and log in with your Inria credentials.
* Click on _SonarQube server_, and then on your account in the upper right.
* Choose _My account_ > _Security_ and generate a token.
* You will need this token throughout the TP; be sure to keep it! For instance store it in an environment variable:

````
export SONARQUBE_TOKEN = XXXXX
````

## Scanner

The first Dockerfile just set up an Ubuntu image with the Heat project and run the vanilla SonarQube scanner instance. To build it, go in _1-Scanner_ directory and type:

````
docker build -t dlp_1_scanner_only --build-arg TOKEN=${TOKEN} . 
````

This will generate two (identical) SonarQube projects:

* sed-sac:heat:dlp:1_scanner_only
* sed-sac:heat:dlp:evoluting

This is only for educational purpose: the latter enables following up the evolution of the Sonar output between two consecutive runs, while the former is useful to remember the state of the previous build.

## Test and coverage

In _2-TestsAndCoverage_ directory:

````
docker build -t dlp_2_test_and_coverage --build-arg TOKEN=${TOKEN} . 
````

## Clang analysis tool

In _3-ClangAnalysis_, we are using the clang analysis tool rather than the compiler in the build. Sone steps of the previous Docker image are therefore slightly modified.

````
docker build -t dlp_3_clang_analysis --build-arg TOKEN=${TOKEN} . 
````

## Additional tools

In _4-MoreTools_, all the tools also used in the TP are added, namely:

* cppcheck
* valgrind
* rats
* vera++

````
docker build -t dlp_4_more_tools --build-arg TOKEN=${TOKEN} . 
````

# For help on Sonarqube@Inria

* An [helpdesk](https://helpdesk.inria.fr/categories/78/show) filing has been created:

<img src="Images/Helpdesk.png" alt="Screenshot helpdesk" width="800"/>

* You may ask help to the local SED: sed-saclay@inria.fr

* If the need arises, we may organize a formation at the end of the year (once we are formed ourselves...)

cat > sonar-project.properties << EOF 
sonar.host.url=https://sonarqube.bordeaux.inria.fr/sonarqube 
sonar.links.homepage=https://gitlab.inria.fr/sed-bso/heat 
sonar.links.scm=https://gitlab.inria.fr/sed-bso/heat.git 
sonar.projectKey=sed-sac:heat:dlp:hack_for_plugin 
sonar.login=`cat ~/.sonarqubetoken` 
sonar.sources=. 
EOF
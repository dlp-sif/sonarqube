cat > sonar-project.properties << EOF 
sonar.host.url=https://sonarqube.bordeaux.inria.fr/sonarqube 
sonar.links.homepage=https://gitlab.inria.fr/sed-bso/heat 
sonar.links.scm=https://gitlab.inria.fr/sed-bso/heat.git 
sonar.projectDescription=Solve the heat propagation equation sonar.projectVersion=1.0 
sonar.login=`cat ~/.sonarqubetoken` 
sonar.scm.disabled=false 
sonar.scm.provider=git 
sonar.sourceEncoding=UTF-8 
sonar.sources=. 
sonar.exclusions=build/CMakeFiles/** 
sonar.language=c 
sonar.c.errorRecoveryEnabled=true 
sonar.c.compiler.parser=GCC 
sonar.c.includeDirectories=$(echo | gcc -E -Wp,-v - 2>&1 | grep "^ " | tr '\n' ',') /home/non_root_user/heat, /home/non_root_user/heat/include, /usr/lib/x86_64-linux-gnu/openmpi/include 
sonar.c.compiler.charset=UTF-8 
sonar.c.compiler.regex=^(.*):(\\\d+):\\\d+: warning: (.*)\\\[(.*)\\\]$ 
EOF